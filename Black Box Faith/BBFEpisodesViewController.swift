//
//  ASEpisodesViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class BBFEpisodesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var contains = NSDictionary()
    var containsDict = NSArray()
    var episodesList = [[String:Any]]()
    var rightHandFocusGuide = UIFocusGuide()
    var UserID,videoId:String!
    var insetButton : UIButton = UIButton()
    var episodeDelegate:myListdelegate!
    var deviceId,uuid: String!
    var storeProdData = [[String:Any]]()
    var Donatedict = [[String:Any]]()
    var isfromsearch = Bool()
    
    @IBOutlet weak var seasontableView: UITableView!
    @IBOutlet weak var eposidetableeView: UITableView!
    @IBOutlet weak var rightHandView: UIView!
    var viewToFocus: UIView? = nil
    {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        insetButton.frame = CGRect(x:630, y: 115, width: 10, height: 10)
        insetButton.backgroundColor = UIColor.clear
        insetButton.layer.borderWidth = 1.0
        insetButton.layer.borderColor = UIColor.black.cgColor
        insetButton.setTitleColor(UIColor.black, for: .normal)
        view.addSubview(insetButton)
        view.addLayoutGuide(rightHandFocusGuide)
        
        rightHandFocusGuide.bottomAnchor.constraint(equalTo: seasontableView.bottomAnchor).isActive = true
        rightHandFocusGuide.leftAnchor.constraint(equalTo: seasontableView.rightAnchor).isActive = true
        rightHandFocusGuide.topAnchor.constraint(equalTo: seasontableView.topAnchor).isActive = true
        rightHandFocusGuide.rightAnchor.constraint(equalTo: eposidetableeView.leftAnchor).isActive = true
        rightHandFocusGuide.preferredFocusedView = insetButton
    }
    
    // Number of Section
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    // Number of Rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var rowCount:Int = 0
        if tableView == self.seasontableView
        {
            rowCount = contains.allKeys.count
        }
        if tableView == self.eposidetableeView
        {
            rowCount = episodesList.count
        }
        return rowCount
    }
    
    // Cell Item at IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        if tableView == self.seasontableView
        {
        
            var tmpSeasonName = "Season"
            var tmpEpisodeName = "Episode"
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            cell.textLabel?.font = UIFont.init(name: "Helvetica-Bold", size: 30)
            cell.textLabel?.textColor = UIColor.white
            cell.layer.cornerRadius = 7.0
            cell.selectionStyle = .none
            episodesList = contains[contains.allKeys[indexPath.row]] as! [[String : Any]]
            if (episodesList.first)?["season_name"] != nil
            {
                if ((episodesList.first)?["season_name"] as! String) != ""
                {
                    tmpSeasonName = ((episodesList.first)?["season_name"] as! String)
                }
            }
            if (episodesList.first)?["episode_name"] != nil
            {
                if ((episodesList.first)?["episode_name"] as! NSArray).count != 0
                {
                    tmpEpisodeName = ((episodesList.first)?["episode_name"] as! NSArray).firstObject as! String
                }
            }
            cell.textLabel?.text = (tmpSeasonName + "  " + "\(contains.allKeys[indexPath.row])" + "       " + " " + tmpEpisodeName + "   " + "\((contains[contains.allKeys[indexPath.row]] as! NSArray).count)")
            
        }
        if tableView == self.eposidetableeView
        {
            
            let episoddss = episodesList.sorted(by: {Int($0["episode_number"] as! String)! < Int($1["episode_number"] as! String)!})
            let path = episoddss[indexPath.row]
            var tmpSeasonName = "Season"
            var tmpEpisodeName = "Episode"
            if path["season_name"] != nil
            {
                if (path["season_name"] as! String) != ""
                {
                    tmpSeasonName = (path["season_name"] as! String)
                }
            }
            if path["episode_name"] != nil
            {
                if ((path["episode_name"] as! NSArray).firstObject as! String) != ""
                {
                    tmpEpisodeName = ((path["episode_name"] as! NSArray).firstObject as! String)
                }
            }
           
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            let img = (cell.viewWithTag(25) as! UIImageView)
            img.kf.indicatorType = .activity
           // (cell.viewWithTag(25) as! UIImageView).kf.setImage(with: URL(string: path["thumb"] as! String))
            if (isnil(json: (path as NSDictionary), key: "thumb")) == ""
            {
                (cell.viewWithTag(25) as! UIImageView).image = UIImage(imageLiteralResourceName: "RevoltFlat")
            }
            else
            {
                (cell.viewWithTag(25) as! UIImageView).kf.setImage(with: URL(string:isnil(json: (path as NSDictionary), key: "thumb")))
            }
            (cell.viewWithTag(24) as! UILabel).text = "\(tmpSeasonName)\(path["season_number"] as! String): \(tmpEpisodeName)\(path["episode_number"] as! String)"
            (cell.viewWithTag(27) as! UIProgressView).isHidden = true
            let DescriptionText = path["description"] as! String
            let destxt = DescriptionText.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
            let destxt1 =  destxt.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
            let destxt2 = destxt1.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
            (cell.viewWithTag(26) as! UITextView).text = destxt2
        }
        return cell
    }
    
    // Did Update focus
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if tableView == self.seasontableView
        {
            guard let nextView = context.nextFocusedView else { return }
            if (nextView == insetButton)
            {
                guard let indexPath = context.previouslyFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath) else
                {
                    return
                }
             //   insetButton.isEnabled = false
                cell.textLabel?.textColor = UIColor.white
                rightHandFocusGuide.preferredFocusedView = cell
              //  viewToFocus = self.eposidetableeView
                
            }
            else
            {
                rightHandFocusGuide.preferredFocusedView = insetButton
            }
            
            guard let indexPath = context.nextFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath)
                else { return }
            cell.textLabel?.textColor = UIColor.black
            episodesList = contains[contains.allKeys[indexPath.row]] as! [[String : Any]]
            eposidetableeView.reloadData()
            
            guard let prevIndexPath = context.previouslyFocusedIndexPath, let prevCell = tableView.cellForRow(at: prevIndexPath)
                else { return }
            prevCell.textLabel?.textColor = UIColor.white
        }
        if tableView == self.eposidetableeView
        {
            if let previousIndexPath = context.previouslyFocusedIndexPath,
                let cell = tableView.cellForRow(at: previousIndexPath)
            {
                (cell.viewWithTag(25) as! UIImageView).transform = .identity
                cell.focusStyle = .custom
                cell.selectionStyle = .none
            }
            if let indexPath = context.nextFocusedIndexPath,
                let cell = tableView.cellForRow(at: indexPath)
            {
                (cell.viewWithTag(25) as! UIImageView).adjustsImageWhenAncestorFocused = true
                cell.focusStyle = .custom
                cell.selectionStyle = .none
            }
        }
    }
    
    // Select item at IndexPath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == self.eposidetableeView
        {
            let path = episodesList[indexPath.row] as NSDictionary
            RecentlyWatched(withurl: kRecentlyWatchedUrl, videoId: path["id"] as! String, userId:self.UserID, row:indexPath)
        }
    }
    
    // Service call for create recently watched
    func RecentlyWatched(withurl:String,videoId:String,userId:String,row:IndexPath)
    {
        let parameters = ["createRecentlyWatched": ["videoId":videoId,"userId":userId]]
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! BBFDetailPageViewController
        BBFApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
//                if (dicton["seekTime"] as! Float64) > 0
//                {
//                    DetailPage.isRecentlywatch = true
//                }
                self.getAssetData(withUrl: kAssestDataUrl, id: dicton["videoId"] as! String, userid: self.UserID,row:row)
                DetailPage.delegate = self.episodeDelegate
                self.episodeDelegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                 let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func getAssetData(withUrl:String,id:String,userid:String,row:IndexPath)
    {
        var parameters =  [String:[String:AnyObject]]()
        var path = NSDictionary()
        _ = eposidetableeView.cellForRow(at: row)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! BBFPlayerLayerViewController

        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        BBFApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
              
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    path = dict.firstObject as! NSDictionary
                }
                else
                {
                     path = JSON as! NSDictionary
                }
                
                var monotize = String()
                var monotizeLabel = String()
                var montizetype = String()
                var montizeURL = String()
                let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! BBFSubscriptionViewController
                
                if path.object(forKey: "metadata") != nil
                {
                    let metaDict = path.object(forKey: "metadata") as! NSDictionary
                    monotize = metaDict["monetize"] as! String
                    monotizeLabel = metaDict["monetize_label"] as! String
                    montizetype = metaDict["monetize_type"] as! String
                }
                else
                {
                    monotize = path["monetize"] as! String
                    monotizeLabel = path["monetize_label"] as! String
                    montizetype = path["monetizetype"] as! String
                }
             
                if path["watchedVideo"] != nil
                {
                    if path["url_m3u8"] != nil && path["url_m3u8"] as! String != ""
                    {
                        PlayerVC.videoUrl = path["url_m3u8"] as! String
                    }
                    else
                    {
                        PlayerVC.videoUrl = path["url"] as! String
                    }
                    
                    PlayerVC.mainVideoID = path["id"] as! String
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.uuid = self.uuid
                    PlayerVC.DonateData = self.Donatedict
                    PlayerVC.storeData = self.storeProdData
                    PlayerVC.userID = self.UserID
                    PlayerVC.videoID = (path["id"] as! String)
                    PlayerVC.monitizeLbl = monotizeLabel
                    PlayerVC.monitizetype = montizetype
                    
                    if path["watchedVideo"] as! Int > 0
                    {
                        PlayerVC.isResume = true
                        PlayerVC.resumeTime = (path["watchedVideo"] as! Float64)
                        
                        //                            print(path["watchedVideo"])
                        //                            print((Int(path["file_duration"] as! String))! / 60)
                        //                            print(Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                        //   (cell?.viewWithTag(27) as! UIProgressView).isHidden = false
                        //   (cell?.viewWithTag(27) as! UIProgressView).progress = (Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                        //    print((cell?.viewWithTag(27) as! UIProgressView).progress)
                    }
                    else
                    {
                        PlayerVC.isResume = false
                        
                    }
                    if monotize == "true"
                    {
                        if (path["monetize_data_source_url"] as! String) == "Products.json"
                        {
                            PlayerVC.isDonate = false
                        }
                        else
                        {
                            PlayerVC.isDonate = true
                            
                        }
                    }
                    else
                    {
                        PlayerVC.isDonate = false
                    }
                    if self.isfromsearch
                    {
                        self.present(PlayerVC, animated: false, completion: nil)
                    }
                    else
                    {
                       self.navigationController?.pushViewController(PlayerVC, animated: false)
                    }
                   
                 //   self.present(PlayerVC, animated: true, completion: nil)
                }
                
               
                
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func myListdata(mylistDict: NSDictionary)
    {
    }
    func removeListdata(id : String)
    {
    }
    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary)
    {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
